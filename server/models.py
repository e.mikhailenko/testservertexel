from django.db import models


class Server(models.Model):
    name = models.CharField(max_length=250)
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.title


class Parameter(models.Model):
    server = models.ForeignKey(
        Server,
        related_name="parameters",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Input(models.Model):
    parameter = models.ForeignKey(
        Parameter,
        related_name="input",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    type = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    title = models.TextField(max_length=1000)

    def __str__(self):
        return self.name


class Item(models.Model):
    input = models.ForeignKey(
        Input,
        related_name="items",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    value = models.CharField(max_length=100)
    title = models.CharField(max_length=200)

    def __str__(self):
        return self.value


class Output(models.Model):
    parameter = models.ForeignKey(
        Parameter,
        related_name="output",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    type = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    title = models.TextField(max_length=1000)
    value = models.TextField(max_length=1000)

    def __str__(self):
        return self.name


class Command(models.Model):
    server = models.ForeignKey(
        Server,
        related_name="commands",
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    name = models.CharField(max_length=100)
    start = models.TextField(max_length=500)

    def __str__(self):
        return self.name
