from django.contrib import admin
from .models import Server, Parameter, Input, Output, Item, Command


class ServerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'title', 'description')


class ParameterAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'server_id')


class InputAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'title', 'parameter_id')


class OutputAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'title', 'parameter_id')


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'value', 'title', 'input_id')


class CommandAdmin(admin.ModelAdmin):
    list_display = ('id', 'start', 'server_id')


admin.site.register(Server, ServerAdmin)
admin.site.register(Parameter, ParameterAdmin)
admin.site.register(Input, InputAdmin)
admin.site.register(Output, OutputAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Command, CommandAdmin)
