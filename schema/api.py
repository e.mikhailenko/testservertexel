import subprocess

import graphene
from graphene_django import DjangoObjectType
import server.models as models


class Server(DjangoObjectType):
    class Meta:
        model = models.Server
        fields = (
            "name",
            "title",
            "description",
            "commands",
            "parameters",
        )


class Parameter(DjangoObjectType):
    class Meta:
        model = models.Parameter
        fields = ("name", "input", "output",)


class Input(DjangoObjectType):
    class Meta:
        model = models.Input
        fields = ("type", "name", "title", "items",)


class Output(DjangoObjectType):
    class Meta:
        model = models.Output
        fields = ("type", "name", "title", "value")


class Item(DjangoObjectType):
    class Meta:
        model = models.Item
        fields = ("value", "title",)


class Command(DjangoObjectType):
    class Meta:
        model = models.Command
        fields = ("name", "start",)


class Query(graphene.ObjectType):
    server = graphene.List(Server, id=graphene.Int())

    def resolve_server(self, info, id):
        return models.Server.objects.filter(id=id)


class UpdateOutput(graphene.Mutation):
    output = graphene.Field(Output)

    class Arguments:
        input_num = graphene.Int()
        input_text = graphene.String()

    def mutate(self, info, input_num, input_text):
        output = models.Output.objects.get(id=1)
        process = subprocess.Popen(
            ['./start.sh %s %s' % (input_num, input_text)],
            shell=True,
            stdout=subprocess.PIPE
        )
        output.value = process.stdout.read()
        output.save(update_fields=["value"])
        return UpdateOutput(output=output)


class Mutation(graphene.ObjectType):
    update_output = UpdateOutput.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
